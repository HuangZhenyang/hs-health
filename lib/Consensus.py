# -*- coding: utf-8 -*
#
# Copyright (C) 2015 - David Goulet <dgoulet@ev0ke.net>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; only version 2 of the License.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA  02111-1307, USA.

from stem import Flag
from stem.descriptor import parse_file

from lib.HSDir import HSDir

class Consensus():
    """
    DOCDOC
    """

    CONSENSUS_DIR_NAME = "consensuses"
    collector_url = "https://collector.torproject.org/recent/relay-descriptors/consensuses/"

    def __init__(self, path=None, tc=None):
        # Contains HSDir() objects indexed by fingerprint.
        self.hsdirs = {}
        # RouterStatusEntryV3 objects in this indexed by fingerprint.
        self.rs_v3 = {}
        # Generator object that contains the consensus from stem.
        consensus = None
        if path is not None:
            # We have a path so use that for our consensus.
            consensus = parse_file(path)
        elif tc is not None:
            # Get local consensus from the given controller.
            consensus = tc.get_consensus()

        # Use the consensus we just get to build our internal state.
        self._consensus_init(consensus)

    def _consensus_init(self, consensus):
        if consensus is None:
            return
        # Build a list of RouterStatusEntryV3 object.
        for elem in consensus:
            self.rs_v3[elem.fingerprint] = elem
        # Build the list of HSDir.
        self._build_hsdirs()

    def _build_hsdirs(self):
        """ DOCDOC """
        # Make sure we work on an empty array.
        for entry in self.rs_v3.values():
            if Flag.HSDIR not in entry.flags:
                continue
            self.hsdirs[entry.fingerprint] = HSDir(entry)

    def get_rs_v3(self, fingerprint):
        """ DOC DOC """
        if fingerprint not in self.rs_v3:
            return None
        return self.rs_v3[fingerprint]

    def get_hsdir(self, fingerprint):
        """ DOC DOC """
        if fingerprint not in self.hsdirs:
            return None
        return self.hsdirs[fingerprint]

    def is_hsdir_alive(self, fingerprint):
        # Not in consensus, consider it not alive.
        return fingerprint in self.hsdirs
