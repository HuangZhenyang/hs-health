# -*- coding: utf-8 -*
#
# Copyright (C) 2015 - David Goulet <dgoulet@ev0ke.net>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; only version 2 of the License.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA  02111-1307, USA.

class HSDir():
    """
    DOCDOC
    """

    def __init__(self, rs_v3):
        # RouterStatusEntryV3 from stem.
        self.rs_v3 = rs_v3
        self.fingerprint = self.rs_v3.fingerprint

    def is_valid(self):
        return self.rs_v3.dir_port is not None
