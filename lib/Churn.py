# -*- coding: utf-8 -*
#
# Copyright (C) 2015 - David Goulet <dgoulet@ev0ke.net>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; only version 2 of the License.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA  02111-1307, USA.

import os, sys

from datetime import datetime, timedelta
from time import time, sleep, strftime, gmtime
from calendar import timegm

from stem import HSDescAction
from stem.control import EventType

class ChurnEffect():
    """
    DOC DOC
    """

    def __init__(self, churn_time, desc_id, hs_addr, hsdirs):
        # What time the churn occured.
        self.time = churn_time
        self.nr_retry = 0

        # Cutoff before we stop scheduling a reachability test is 2 hours after
        # the churn. If in that period all HSDir in the invalid_hsdirs table
        # don't have the desc_id, it's a serious reachability issue.
        self.cutoff = churn_time + (3 * 60 * 60)

        # For which descriptor ID this happened.
        self.desc_id = desc_id
        # Onion address
        self.hs_addr = hs_addr
        # Set of HSDirs that have the descriptor according to the latest
        # consensus.
        self.hsdirs = hsdirs
        # Set of HSDirs NOT having the descriptor but should have according to
        # the latest consensus.
        self.invalid_hsdirs = {}

    def remove_valid_hsdir(self, hsdir):
        """ Remove valid HSdir from the list and put in the invalid list. """
        if hsdir.fingerprint in self.hsdirs:
            self.hsdirs.pop(hsdir.fingerprint)

    def schedule_event(self, context, hsdir):
        """ DOC DOC """
        sched = context.scheduler
        now = time()
        self.nr_retry += 1
        dir_fp = hsdir.fingerprint
        if now > self.cutoff:
            context.log.info("[%s] HSDir %s still failing for ID %s "
                             "for now %d seconds. Giving up!" % \
                             (self.hs_addr, dir_fp, self.desc_id,
                                 now - self.time))
            self.invalid_hsdirs.pop(dir_fp)
            return
        hsdirs = {dir_fp: hsdir}
        # Run fetch test every 15 minutes. We'll do that until the cutoff.
        next_run_time = datetime.utcnow() + timedelta(minutes=15)
        job_id = "%s-%s-churn" % (dir_fp, self.desc_id)
        if sched.get_job(job_id) is not None:
            return
        sched.add_job(context.tor_controller.fetch_hs_descriptor, 'date',
                run_date=next_run_time, kwargs = {'log_addr': self.hs_addr,
                    'desc_id': self.desc_id, 'hsdirs': hsdirs}, id = job_id)

class FailedFetch():
    """
    Object used to track failed fetch. We retry bunch of times to measure
    reachability of an HS descriptor. Also provides a log function that save
    the information in CSV format.
    """

    # We retry every 15 minutes because this is the maximum time an HS will
    # take to re-upload a new version of the descriptor. Keep the max at 4
    # hours thus 16 tries.
    MAX_FETCH_FAILED = 4 * (60 / 15)

    # Number of minutes we should retry a fetch that failed.
    RETRY_DELAY = 15

    def __init__(self, hs, hsdir, desc_id, log_fn):
        # First fail timestamp
        self.first_time = 0
        # Last fail timestamp
        self.last_time = 0
        self.fail_count = 0
        # The HS for which this object is used on.
        self.hs = hs
        self.hsdir = hsdir
        # Which descriptor ID of the self.hs is failing.
        self.desc_id = desc_id
        # Every fail count, add the reason in this table.
        self.reasons = []
        self.log = log_fn

    def fail(self, reason):
        now = time()
        if self.first_time == 0:
            self.first_time = now
        self.last_time = now
        self.fail_count += 1
        self.reasons.append(reason)
        self.log("[%s] Fail fetch for ID %s on HSDir %s with reason %s" % \
                 (self.hs.get_onion_address(), self.desc_id,
                  self.hsdir.fingerprint, reason))

    def log(self, context, success_ts=None, hsdir_ts=None):
        context.csv.write([self.desc_id, self.hsdir.fingerprint,
            self.hs.get_onion_address(), self.hs.desc_id_time,
            hsdir_ts, self.first_time, self.last_time, self.fail_count,
            success_ts, self.reasons[-1]])


class Analysis():
    """
    DOC DOC
    """

    def __init__(self, hs):
        # Hidden service we are analyzing. One per object.
        self.hs = hs
        # Get a pointer to the context so we can use it easily.
        self.context = self.hs.context
        # ChurnEffect object that happened during the analysis indexed by
        # desc_id.
        self.churn_effects = {}
        # Failed fetches object indexed by HSDir fingerprint.
        self.failed_fetches = {}
        # Add HS Descriptor event to tor controller.
        self.context.tor_controller.add_event(self._hs_desc_handler,
                                              EventType.HS_DESC)

    def info(self, msg):
        self.context.log.info(msg)

    def _get_failed_fetch(self, dir_fp):
        ff = None
        if dir_fp in self.failed_fetches:
            ff = self.failed_fetches[dir_fp]
        return ff

    def _remove_timeout_job(self, dir_fp, desc_id):
        # Remove pending timeout desc event job.
        job_id = "%s-%s-timeout" % (dir_fp, desc_id)
        if self.context.scheduler.get_job(job_id) is not None:
            self.context.scheduler.remove_job(job_id)

    def _sched_timeout_job(self, event):
        sched = self.context.scheduler
        # Build a specific timeout job id so it can be removed on success/fail.
        job_id = "%s-%s-timeout" % \
                (event.directory_fingerprint, event.descriptor_id)
        if sched.get_job(job_id) is not None:
            self.info("[%s] Timeout job %s was already queued" % \
                      (self.hs.get_onion_address(), job_id))
            return
        # We have requested a descriptor, let's schedule a timeout event and
        # after 120 seconds log the potential issue.
        next_dtime = datetime.utcnow() + timedelta(seconds=120)
        sched.add_job(self._timeout_desc_event, 'date', run_date=next_dtime,
                kwargs = {'event': event}, id=job_id)

    def _sched_failed_fetch(self, fail_fetch):
        """
        Reschedule a fetch to see if this is a one time thing. For instance the
        HSDir could have just restarted, cleaning its cache. HS reupload every
        15 minutes so do that in 15 minutes.
        """
        # Ease our life, get some info in shorter variables.
        hsdir = fail_fetch.hsdir
        sched = self.context.scheduler
        onion_addr = fail_fetch.hs.get_onion_address()
        desc_id = fail_fetch.desc_id

        if hsdir is None:
            # *NOT* suppose to happen.
            self.info("[%s] Meh, no HSDir for ID %s not found..." % \
                      (onion_addr, desc_id))
            return
        dir_fp = hsdir.fingerprint
        job_id = "%s-%s-failed" % (dir_fp, desc_id)
        # First, check if we already have a failed fetch scheduled to avoid
        # redundant check. This can happen if we have a new consensus event for
        # instance.
        if sched.get_job(job_id) is not None:
            return
        # Don't go over the limit else if the HSDir is really broken, this will
        # loop for a long time.
        if fail_fetch.fail_count >= FailedFetch.MAX_FETCH_FAILED:
            self.info("[%s] HSDir %s failed for %d sec. on ID %s. Giving up." % \
                      (onion_addr, dir_fp, time() - fail_fetch.first_time,
                       desc_id))
            fail_fetch.log(self.context, hsdir_ts=self.hs.desc_id_time)
            # Giving up, remove it and stop retrying.
            self.failed_fetches.pop(dir_fp)
            return
        # Ok, schedule a fetch in 15 minutes.
        next_dtime = datetime.utcnow() + timedelta(minutes=FailedFetch.RETRY_DELAY)
        sched.add_job(self.context.tor_controller.fetch_hs_descriptor, 'date',
                run_date=next_dtime,
                kwargs = {
                    'log_addr': onion_addr,
                    'desc_id': desc_id,
                    'hsdirs': {dir_fp : hsdir}},
                id = job_id)
        self.info("[%s] Rescheduling fetch of ID %s on HSDir %s in %d minutes" % \
                  (onion_addr, desc_id, dir_fp, FailedFetch.RETRY_DELAY))

    def _timeout_desc_event(self, event):
        """
        Function that logs an event for which we haven't receive back a
        RECEIVED/FAILED control event indicating a possible issue.
        """
        dir_fp = event.directory_fingerprint
        desc_id = event.descriptor_id
        hsdir = self.hs.find_hsdir(dir_fp)

        # Are we dealing with a churn effect?
        if desc_id in self.churn_effects:
            ce = self.churn_effects[desc_id]
            ce.schedule_event(self.context, hsdir)
            self.info("[%s] Churn - HSDir %s request timed out." % \
                      (self.hs.get_onion_address(), dir_fp))
            return
        # Do we have a failed fetch for that and if not create one.
        ff = self._get_failed_fetch(dir_fp)
        if ff is None:
            ff = FailedFetch(self.hs, hsdir, desc_id, self.info)
        ff.fail(event.reason)
        # Reschedule a fetch to see if this is a one time thing or not.
        self._sched_failed_fetch(ff)

    def _requested_desc_event(self, event):
        """
        Control HSDESC requested event handler.
        """
        # Requested descriptor detected so schedule a timeout on it.
        self._sched_timeout_job(event)

    def _received_desc_event(self, event):
        now = time()
        dir_fp = event.directory_fingerprint
        desc_id = event.descriptor_id
        # Remove any timeout scheduled event.
        self._remove_timeout_job(dir_fp, desc_id)

        # Are we receiving an event for a churn effect?
        if desc_id in self.churn_effects:
            self.handle_churn_received(event, now)
            return

        # Check if we have a FailedFetch object for this HSDir and if so log
        # the recover and remove the object.
        ff = self._get_failed_fetch(dir_fp)
        if ff is not None:
            ff.log(self.context, success_ts=now, hsdir_ts=self.hs.desc_id_time)
            self.info("[%s] HSDir %s finally succeeded for ID %s after %d seconds" % \
                      (ff.hs.get_onion_address(), ff.hsdir.fingerprint, ff.desc_id,
                       now - ff.first_time))
            # No need to keep it, quiescent state for the HSDir.
            self.failed_fetches.pop(dir_fp)
        else:
            # Ok this means we did a fetch and we got it right away.
            self.context.csv.write([desc_id, dir_fp,
                self.hs.get_onion_address(), self.hs.desc_id_time,
                self.hs.desc_id_time, None, None, 0, now, None])

    def _failed_desc_event(self, event):
        """ Control HSDESC failed event. """
        # Ease our life a bit.
        desc_id = event.descriptor_id
        dir_fp = event.directory_fingerprint

        # Remove any timeout event that are scheduled for this.
        self._remove_timeout_job(dir_fp, desc_id)
        if desc_id in self.churn_effects:
            # This is a failure due to churn. Handle it.
            self.handle_churn_failed(event)
        else:
            ff = self._get_failed_fetch(dir_fp)
            if ff is None:
                hsdir = self.hs.find_hsdir(dir_fp)
                ff = FailedFetch(self.hs, hsdir, desc_id, self.info)
            ff.fail(event.reason)
            self._sched_failed_fetch(ff)

    def _hs_desc_handler(self, event):
        """ HS Descriptor event handler from the tor controller. """
        # Avoid handling a request that is not for our HS.
        if event.descriptor_id not in self.hs.desc_id:
            return

        if event.action == HSDescAction.REQUESTED:
            self._requested_desc_event(event)
        elif event.action == HSDescAction.RECEIVED:
            self._received_desc_event(event)
        elif event.action == HSDescAction.FAILED:
            self._failed_desc_event(event)
        else:
            self.info("Unknown event action %s" % (event.action))

    def handle_churn_received(self, event, now):
        """ DOC DOC """
        dir_fp = event.directory_fingerprint
        desc_id = event.descriptor_id
        ce = self.churn_effects[desc_id]
        if dir_fp in ce.invalid_hsdirs:
            # Success finally! Remove it from invalid hsdir.
            ce.invalid_hsdirs.pop(dir_fp)
            self.info("[%s] Churn, ID %s finally on HSDir %s after %d seconds" % \
                      (self.hs.get_onion_address(), desc_id, dir_fp, now - ce.time))
            self.context.csv.write([desc_id, dir_fp,
                self.hs.get_onion_address(), self.hs.desc_id_time,
                ce.time, ce.time, now, ce.nr_retry, now, "NOT_FOUND"])
        else:
            # Success right away for this new HSDir
            self.context.csv.write([desc_id, dir_fp,
                self.hs.get_onion_address(), self.hs.desc_id_time,
                ce.time, None, None, 0, now, None])

    def handle_churn_failed(self, event):
        """ DOC DOC """
        desc_id = event.descriptor_id
        dir_fp = event.directory_fingerprint

        if desc_id not in self.churn_effects:
            self.info("[fatal] Descriptor ID %s not in churn effect " % \
                     (desc_id))
            return

        ce = self.churn_effects[desc_id]
        if dir_fp in ce.invalid_hsdirs:
            # Could be that we are retrying a previously invalid HSDir.
            hsdir = ce.invalid_hsdirs[dir_fp]
        elif dir_fp in ce.hsdirs:
            # Ok first fail of a valid HSDir.
            hsdir = ce.hsdirs[dir_fp]
            # Move it to invalid list and remove it from the valid one.
            ce.invalid_hsdirs[dir_fp] = hsdir
            ce.remove_valid_hsdir(hsdir)
        else:
            # Not good we got a fail from a non existent HSDir in the churn.
            self.info("[fatal] %s not in churn effect HSDirs" % (dir_fp))
            return

        self.info("[%s] HSDir %s didn't have desc ID %s" % \
                 (self.hs.get_onion_address(), dir_fp, desc_id))

        # Schedule next event of this churn effect.
        ce.schedule_event(self.context, hsdir)

    def handle_churn(self, now, consensus, desc_id, current_hsdirs, new_hsdirs):
        """ DOC DOC """

        hs_onion_addr = self.hs.get_onion_address()

        # Union of all possible HSDirs for the given descriptor ID.
        union_hsdirs = dict(current_hsdirs.items() + new_hsdirs.items())

        removed_hsdirs = self.context.diff_hsdirs(current_hsdirs, new_hsdirs)
        added_hsdirs = self.context.diff_hsdirs(new_hsdirs, current_hsdirs)

        self.info("[%s] Churn occured. For descriptor ID %s. " \
                  "HSDir(s):\n  %s -->\n  %s" % \
                 (hs_onion_addr, desc_id, removed_hsdirs.keys(),
                  added_hsdirs.keys()))

        # Make sure the removed HSDir(s) are still alive or not so we don't run
        # analysis on dead relays.
        self.clean_hsdirs(consensus, union_hsdirs)

        # Add a ChurnEffect object indexed by directory fingerprint so once all
        # the test events happen, we'll log and remove it.
        if desc_id not in self.churn_effects:
            ce = ChurnEffect(now, desc_id, hs_onion_addr, union_hsdirs)
            self.churn_effects[desc_id] = ce

        # Trigger a fetch descriptor on all HSDirs to make sure they are all
        # reachable.
        self.context.tor_controller.fetch_hs_descriptor(log_addr=hs_onion_addr,
                desc_id=desc_id, hsdirs=union_hsdirs)

    def schedule_events(self):
        """ DOC DOC """
        # Next hour rounded up.
        next_hour = self.context.get_next_hour_time()
        # Ease our life.
        sched = self.context.scheduler

        # New consensus available on CollecTor at the next hour plus 8 minutes.
        # It's usually available between 6 and 12 minutes after the hour,
        # depends a lot on CollecTor load and clock skew.
        next_time = next_hour + (8 * 60)
        # Make a unique job ID using the onion address.
        job_id = "churn-%s-%s" % (self.hs.get_onion_address(), next_time)
        if sched.get_job(job_id) is not None:
            # Just in case, schedule only once.
            return
        sched.add_job(self.run, 'date', \
                      run_date=datetime.utcfromtimestamp(next_time),
                      kwargs = {'use_collector': True},
                      id = job_id)

    def garbage_collect(self):
        """
        Remove referenced object but that expired or are not valid anymore.
        """
        # First, check the churn effect object.
        for ce in self.churn_effects.values():
            if len(ce.invalid_hsdirs) == 0:
                self.churn_effects.pop(ce.desc_id)
        # Second, removed failed fetch object that tried way too much.
        for ff in self.failed_fetches.values():
            if ff.fail_count >= FailedFetch.MAX_FETCH_FAILED:
                self.failed_fetches.pop(ff.hsdir.fingerprint)

    def clean_hsdirs(self, consensus, hsdirs):
        """
        Take the hsdirs dict and check if they are valid and alive against the
        given consensus. If one of the condition fails, remove it from the
        given dict.
        """
        for fp in hsdirs:
            if consensus.is_hsdir_alive(fp):
                continue
            if hsdirs[fp].is_valid():
                continue
            hsdirs.pop(fp)

    def run(self, use_collector=False, do_sched=True):
        """
        Run churn analysis. This function will reschedule events by itself once
        it's run at least once if do_sched is True.
        """
        # We need to update the desriptor ID once the full replica loop is done
        # so flag this when we need to.
        update_desc_id = False
        # Ease our life.
        hs = self.hs
        nr_replica = hs.REND_NUMBER_OF_NON_CONSECUTIVE_REPLICAS
        now = time()

        self.garbage_collect()
        consensus = self.context.get_latest_consensus(use_collector)

        # For each HS's descriptor ID, check for churn effect.
        for rep in range(0, nr_replica):
            # Compute the descriptor ID value at of now and get the responsible
            # HSDir for it.
            new_desc_id = hs.get_desc_id(rep, now=now)
            new_hsdirs = hs.get_responsible_hsdir(new_desc_id, consensus.hsdirs)

            # Get the current descriptor ID and HSDirs for it.
            current_hsdirs = hs.hsdirs[rep]
            current_desc_id = hs.get_current_desc_id(rep)

            # Update HS hsdirs list with the latest.
            hs.update_hsdirs(rep, new_hsdirs)

            if current_desc_id == new_desc_id:
                # Same descriptor ID, diff hsdirs for churn.
                diff = self.context.diff_hsdirs(new_hsdirs, current_hsdirs)
                if len(diff) > 0:
                    # We have a churn effect here so analyze it.
                    self.handle_churn(now, consensus, new_desc_id,
                            current_hsdirs, new_hsdirs)
                else:
                    # Trigger a fetch descriptor on all HSDirs to make sure
                    # they are all reachable.
                    self.context.tor_controller.fetch_hs_descriptor(
                            self.hs.get_onion_address(), desc_id=new_desc_id,
                            hsdirs=new_hsdirs)
            else:
                # Reset state because we have a whole new set of HSDirs and
                # descriptor ID.
                update_desc_id = True
                # Try old HSDir with the old descriptor ID to see if they are
                # still reachable for people without this consensus.
                if len(current_hsdirs) > 0:
                    # Just make sure we don't probe dead relays.
                    self.clean_hsdirs(consensus, current_hsdirs)
                    self.context.tor_controller.fetch_hs_descriptor(
                            self.hs.get_onion_address(),
                            desc_id=current_desc_id, hsdirs=current_hsdirs)
                # Try new HSDirs with new descriptor ID.
                self.context.tor_controller.fetch_hs_descriptor(
                        self.hs.get_onion_address(), desc_id=new_desc_id,
                        hsdirs=new_hsdirs)

        if update_desc_id is True:
            hs.update_desc_id()

        if do_sched is True:
            # Scheduled events for the next set of analysis.
            self.schedule_events()
