# -*- coding: utf-8 -*
#
# Copyright (C) 2015 - David Goulet <dgoulet@ev0ke.net>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; only version 2 of the License.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA  02111-1307, USA.

import struct

from base64 import b32decode, b32encode
from bisect import bisect_right
from calendar import timegm
from hashlib import sha1
from time import time, mktime, localtime, gmtime

from datetime import datetime, timedelta
from time import time, sleep, asctime
from calendar import timegm

import lib.Churn as Churn

class HiddenService():
    """
    DOCDOC
    """

    # Taken from the tor ABI in or.h
    REND_TIME_PERIOD_V2_DESC_VALIDITY = 24*60*60
    REND_NUMBER_OF_NON_CONSECUTIVE_REPLICAS = 2
    REND_NUMBER_OF_CONSECUTIVE_REPLICAS = 3

    def __init__(self, onion_addr, context):
        # Context which contains logging and scheduler among other things.
        self.context = context
        # Original onion address without the .onion. It's base32 encoded.
        self.b32_encoded_addr = onion_addr
        # Base32 decoded onion address. Useful for later use.
        self.b32_decoded_addr = b32decode(onion_addr.upper())

        # Log info function
        self.log = context.log.info

        # Current descriptor ID indexed by replica.
        self.desc_id = []
        # Timestamp when the current desc IDs was first set.
        self.desc_id_time = 0
        self.next_descid_time = 0
        self.update_desc_id()

        consensus = context.get_latest_consensus(use_collector=False)
        # Responsible HSDirs for the current descriptor ID.
        self.hsdirs = [
            self.get_responsible_hsdir(self.desc_id[0], consensus.hsdirs),
            self.get_responsible_hsdir(self.desc_id[1], consensus.hsdirs)
        ]

        self.analysis = Churn.Analysis(self)

    def update_desc_id(self):
        now = time()
        self.desc_id = [
            self.get_desc_id(0, now=now),
            self.get_desc_id(1, now=now)
        ]
        self.desc_id_time = now
        self.next_descid_time = self.get_next_desc_id()['time']
        self.log("[%s] Descriptor ID set to: %s" % \
                 (self.get_onion_address(), self.desc_id))
        self.log("[%s] Next descriptor ID in %d minutes" % \
                 (self.get_onion_address(), (self.next_descid_time - now) / 60))

    def update_hsdirs(self, replica, new_hsdirs):
        self.hsdirs[replica] = new_hsdirs

    def find_hsdir(self, fp):
        """
        Check in our HSDirs list for fp and if found return it else None.
        """
        for dirs in self.hsdirs:
            if fp in dirs:
                return dirs[fp]
        return None

    def analyze(self):
        """ DOC """
        # Run analysis
        self.analysis.run(use_collector=False)

    def get_onion_address(self):
        return self.b32_encoded_addr

    def get_current_desc_id(self, replica):
        return self.desc_id[replica]

    def _get_time_period(self, now=None):
        """ Return time period for an HS descriptor. """

        # Get permanent-id-byte from the base32 onion address.
        perm_id_byte = ord(self.b32_decoded_addr[0]) & 0xff
        # Remove floating part of the current time.
        if now is None:
            now = int(time())
        # Ease our life a bit. 
        validity = HiddenService.REND_TIME_PERIOD_V2_DESC_VALIDITY

        # Remove float.
        time_period = int((now + (perm_id_byte * validity / 256)) / validity)
        #print("time_period: %d" % (time_period))
        return time_period

    def _get_secret_id(self, replica, now=None):
        """ Return secred ID part. """

        time_period = self._get_time_period(now=now)

        # Return network ordered time period.
        payload = struct.pack('>LB', time_period, replica)

        secret_id_digest = sha1()
        secret_id_digest.update(payload)

        return secret_id_digest.digest()

    def get_desc_id(self, replica, now=None):
        """ Return the descriptor ID of this HS for the given replica. """

        secret_id = self._get_secret_id(replica, now=now)

        desc_id = sha1()
        desc_id.update(self.b32_decoded_addr)
        desc_id.update(secret_id)

        return b32encode(desc_id.digest()).lower()

    def get_responsible_hsdir(self, desc_id, hsdirs):
        """
        Return a list of HSDir object responsible for the given descriptor ID.
        """

        responsible_dirs = {}
        sorted_dir_fp = sorted(hsdirs)
        desc_id_hex = b32decode(desc_id.upper()).encode('hex').upper()

        for i in range(0, HiddenService.REND_NUMBER_OF_CONSECUTIVE_REPLICAS):
            # Get index of first HSDir to the right of the desc id. If we are at
            # the end, go around to the first one.
            idx = (bisect_right(sorted_dir_fp, desc_id_hex) % len(hsdirs)) + i
            dir_fp = sorted_dir_fp[idx]
            # XXX: Let's avoid querying unsuable HSDir (#15801).
            if hsdirs[dir_fp].is_valid():
                responsible_dirs[dir_fp] = hsdirs[dir_fp]

        return responsible_dirs

    def get_next_desc_id(self):
        """ 
        Return a dict the next time the descriptor ID will change
        followed by the replica 0 and 1 desc id value.
        """
        current_period = self._get_time_period()
        next_hour = self.context.get_next_hour_time()

        # Check at each hour for the validity of a descriptor.
        start = next_hour
        step = 60 * 60
        end = (26 * step) + start

        for seconds in range(start, end, step):
            period = self._get_time_period(seconds)
            if period == current_period:
                continue
            # We have a descriptor ID change!
            d = {
                'time': seconds,
                'desc_id': [
                    self.get_desc_id(0, now = seconds),
                    self.get_desc_id(1, now = seconds),
                ]
            }
            return d
