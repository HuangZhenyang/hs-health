# -*- coding: utf-8 -*
#
# Copyright (C) 2015 - David Goulet <dgoulet@ev0ke.net>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; only version 2 of the License.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA  02111-1307, USA.

import sys, os
import logging
import urllib
import csv

from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime, timedelta
from time import time, sleep, gmtime, strftime
from calendar import timegm
from pytz import utc
from threading import Lock
from tempfile import mktemp

from lib.TorController import TorController
from lib.HiddenService import HiddenService
from lib.Consensus import Consensus

# Onion addresses to analyze.
onion_list = [
    "wlupld3ptjvsgwqw", # Wikileaks
    #"agorahooawayyfoe", # Marketplace
    #"4cjw6cwpeaeppfqz", # Riseup XMPP
    #"facebookcorewwwi", # Facebook
    #"3g2upl4pq6kufc4m", # DuckDuckGo
]

class CSVWriter():
    """
    DOC DOC
    """

    def __init__(self):
        self.filename = "health.csv"
        self.fields = ['desc_id', 'hsdir_fp', 'onion_addr', 'ts_first_used_descid',
                'ts_hsdir_start_resp', 'ts_first_fail', 'ts_last_fail',
                'total_fail', 'ts_first_success', 'last_fail_reason']
        self.csv_fd = None
        self.writer = None
        self._open_csv_file()

    def _open_csv_file(self):
        self.csv_fd = open(self.filename, 'a+')
        self.writer = csv.DictWriter(self.csv_fd, fieldnames=self.fields)

    def close(self):
        if self.csv_fd is not None:
            self.csv_fd.close()
        self.csv_fd = None

    def write(self, args):
        # Make sure we don't have any None so replace them by "na".
        for n, elem in enumerate(args):
            if elem == None:
                args[n] = "na"

        data = { self.fields[0] : args[0],
                 self.fields[1] : args[1],
                 self.fields[2] : args[2],
                 self.fields[3] : args[3],
                 self.fields[4] : args[4],
                 self.fields[5] : args[5],
                 self.fields[6] : args[6],
                 self.fields[7] : args[7],
                 self.fields[8] : args[8],
                 self.fields[9] : args[9] }
        self.writer.writerow(data)
        # To be able to read the results as they are coming in.
        self.csv_fd.flush()

class Context():
    """
    DOCDOC
    """

    def __init__(self, onions):
        # Log everything at INFO level.
        hdlr = logging.FileHandler("health.log")
        fmt = logging.Formatter("%(asctime)s - %(message)s", \
                                datefmt="%Y-%m-%d %H:%M:%S")
        fmt .converter = gmtime
        hdlr.setFormatter(fmt)
        self.log = logging.getLogger("health")
        self.log.setLevel(logging.INFO)
        self.log.addHandler(hdlr)
        # Make sure we don't parse nor download a consensus at the same time
        # which creates a race for multiple thread trying to parse/fetch the
        # same file and ends up with partial consensus.
        self.consensus_lock = Lock()
        # Event scheduler.
        self.scheduler = BackgroundScheduler()
        self.scheduler.configure(timezone=utc)
        self.scheduler.start()
        # Set our logger into the Scheduler.
        sched_logger = logging.getLogger("apscheduler.executors.default")
        sched_logger.setLevel(logging.DEBUG)
        sched_logger.propagate = False

        # HS Churn Analysis object list.
        self.services = []
        self.tor_controller = TorController(self)
        # Init phase.
        self._init_services(onions)
        self.csv = CSVWriter()

    def _init_services(self, onions):
        """ Init the HS list using the given onions list. """
        for onion in onions:
            self.services.append(HiddenService(onion, self))

    def cleanup(self):
        self.scheduler.shutdown()
        self.csv.close()
        self.tor_controller.close()

    def get_collector_filename(self, time=None):
        time_struct = gmtime(time)
        return strftime("%Y-%m-%d-%H-00-00-consensus", time_struct)

    def fetch_collector(self, filename, save_filename=None):
        """
        Fetch a consensus on CollecTor. Return filename where the consensus
        has been saved.
        """
        request = urllib.urlopen(Consensus.collector_url + filename)
        if request.code != 200:
            # Most probably the file is not found.
            raise IOError

        if save_filename is not None:
            save_to = save_filename
        else:
            save_to = mktemp()

        fp = open(save_to, "wb")
        fp.write(request.read())
        fp.close()
        return save_to

    def get_latest_consensus(self, use_collector):
        self.consensus_lock.acquire()
        if use_collector is False:
            self.consensus_lock.release()
            return Consensus(path=None, tc=self.tor_controller)

        # Let's ask CollecTor for this one.
        if not os.path.isdir(Consensus.CONSENSUS_DIR_NAME):
            os.mkdir(Consensus.CONSENSUS_DIR_NAME)

        # Get latest consensus using the previous hour time. If it's 17h03,
        # the 17h00 hour consensus will be used. Note that on CollecTor,
        # consensus are only available 6 minutes after the hour.
        cons_name = self.get_collector_filename(self.get_prev_hour_time())
        save_path = "%s/%s" % (Consensus.CONSENSUS_DIR_NAME, cons_name)
        if not os.path.isfile(save_path):
            fetched = False
            # Give up after 20 retries seperated by 30 seconds (10 minutes)
            # each to avoid DoSing CollecTor or stall the process especially
            # because we are holding a lock.
            for retry in range(0, 20):
                # Fetch the consensus file if not found on the local filesystem.
                try:
                    self.fetch_collector(cons_name, save_filename=save_path)
                    fetched = True
                    self.log.info("Using consensus %s fetched on CollecTor" % (cons_name))
                    break
                except IOError, e:
                    self.log.info("Consensus %s was not found." % (cons_name))
                    sleep(30)
            if fetched is False:
                self.log.info("Fatal: CollecTor is unavailable!")
                self.consensus_lock.release()
                return Consensus(path=None, tc=self.tor_controller)

        consensus = Consensus(path=save_path, tc=self.tor_controller)

        self.consensus_lock.release()
        return consensus

    def get_next_hour_time(self):
        """ Helper function to get the time of the next hour. """
        next_date = datetime.utcnow().replace(minute=0, second=0, microsecond=0) \
                + timedelta(hours = 1)
        return timegm(next_date.timetuple())

    def get_prev_hour_time(self):
        """ Helper function to get the time of the previous hour. """
        next_date = datetime.utcnow().replace(minute=0, second=0, microsecond=0)
        return timegm(next_date.timetuple())

    def diff_hsdirs(self, a, b):
        """ Return the items that are in a but not in b. """
        return {x: a[x] for x in a if x not in b}


def heartbeat(context):
    """ DOC DOC """
    log = context.log.info

    # Print the amount of scheduled jobs.
    log("[heartbeat] Scheduled jobs count: %d" % \
            (len(context.scheduler.get_jobs())))
    #context.scheduler.print_jobs()

def start_analysis(context):
    """ DOC DOC """
    for hs in context.services:
        hs.analyze()

def main():

    # Create the context object for the whole execution.
    context = Context(onion_list)

    try:
        start_analysis(context)
        while True:
            heartbeat(context)
            # Every 30 minutes, heartbeat so we can know what's going on.
            sleep(30 * 60)
    except KeyboardInterrupt:
        print("Stopping...")
        context.cleanup()
        sys.exit(0)

if __name__ == '__main__':
    main()
