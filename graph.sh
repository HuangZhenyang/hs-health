#!/bin/bash

python dump.py

cd graph/

for r_file in `find . -name "*.R"`; do
	R --slave -f $r_file >/dev/null 2>&1
	filename=$(echo "$r_file" | awk '{gsub(/.*[/]|[.]{1}[^.]+$/, "", $0)} 1')
	echo "Generated in ${filename}.png"
done
